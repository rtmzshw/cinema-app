import mongoose from "mongoose"
import Joi from "joi"

const moviesSchema = new mongoose.Schema({
    name: String,
    src: String,
    year: Number,
    rating: Number,
    price: Number
});


export const moviesModel = mongoose.model('movies', moviesSchema);

export const movieQueryJoiSchema = Joi.object({
    id: Joi.string().optional(),
    name: Joi.string().optional(),
    rank: Joi.number().optional(),
    price: Joi.object({
        min: Joi.number().required(),
        max: Joi.number().required()
    }).optional(),
    byYear: Joi.boolean().optional()
}).unknown()

export const postMovieBodyJoiSchema = Joi.object({
    name: Joi.string().required(),
    src: Joi.string().required(),
    rank: Joi.number().required(),
    price: Joi.number().required(),
    year: Joi.number().required()
}).unknown()