import { Request, Response, Router } from "express"
import { movieQueryJoiSchema, moviesModel, postMovieBodyJoiSchema } from "./movie.schema"

export const moviesRouter = Router()

moviesRouter.get('/byId/:id', async (req: Request, res: Response) => {
    const movieId = req.params.id;
    const movie = await moviesModel.findById(movieId).lean();
    if (movie) {
        res.json(movie);
    } else {
        console.log("not found");
        res.sendStatus(404);
    }
})

moviesRouter.get('/', async (req: Request, res: Response) => {
    const { q } = req.query;
    let query: any = [];
    if (q) {
        const { error, value } = movieQueryJoiSchema.validate(q);

        if (error) {
            console.log("movie query validation error");
            console.log(JSON.stringify(error));
            return res.sendStatus(400);
        }

        const { id, name, price, rank, byYear } = value;

        if (id)
            query.push({ $match: { "_id": id } })

        if (name)
            query.push({ $match: { "name": { $regex: name } } })

        if (rank)
            query.push({ $match: { "rating": { $gte: rank } } })

        if (price)
            query.push({ $match: { "price": { $gte: price.min, $lte: price.max } } })

        if (byYear)
            query.push(
                {
                    $project: {
                        name: 1,
                        src: 1,
                        year: 1,
                        rating: 1,
                        price: 1,
                        decade: { $round: [{ $divide: ["$year", 10] }, 0] }

                    }
                },
                {
                    $group: {
                        _id: "$decade",
                        movies: { $push: "$$ROOT" }
                    }
                })

        const result = await moviesModel.aggregate(query);
        res.status(200).json(result)
    } else {
        
        const result = await moviesModel.find({})

        res.status(200).json(result)
    }

})

moviesRouter.post('/', async (req: Request, res: Response) => {
    const { error, value } = postMovieBodyJoiSchema.validate(req.body);
    if (error) {
        console.log("post movie body validation error");
        console.log(JSON.stringify(error));
        return res.sendStatus(400);
    }
    await moviesModel.insertMany([value]);
    res.status(200).send()
})

moviesRouter.put('/:id', async (req: Request, res: Response) => {
    const { id } = req.params
    let updateQuery: any = {}
    if (req.body.name)
        updateQuery.name = req.body.name

    if (req.body.src)
        updateQuery.src = req.body.src

    await moviesModel.updateOne({ _id: id }, updateQuery)
    res.status(200).send()
})

moviesRouter.delete('/:id', async (req: Request, res: Response) => {
    const { id } = req.params
    await moviesModel.remove({ _id: id })
    res.status(200).send()
})