import { Request, Response, Router } from "express"
import { postScreeningBodyJoiSchema, screeningModel, screeningQueryJoiSchema } from "./screening.schema"
import { dissoc, repeat } from "ramda"

export const screeningRouter = Router()

screeningRouter.get('/', async (req: Request, res: Response) => {
    const { q } = req.query
    let query: any = {}

    if (q) {
        const { error, value } = screeningQueryJoiSchema.validate(q);

        if (error) {
            console.log("screening query validation error");
            console.log(JSON.stringify(error));
            return res.sendStatus(400);
        }

        const { movieId, isVip, time } = value;

        if (movieId) {
            query.movieId = movieId;
        }

        if (isVip)
            query.isVip = isVip;

        if (time)
            query.time = { $gte: time.min, $lte: time.max };

    }
    const result = await screeningModel.find(query);
    res.json(result);
})

screeningRouter.post('/', async (req: Request, res: Response) => {
    const { error, value } = postScreeningBodyJoiSchema.validate(req.body);
    if (error) {
        console.log("post screening body validation error");
        console.log(JSON.stringify(error));
        return res.sendStatus(400);
    }
    const seatsMap = theatreSizeToSeatsMap(value.theatreSize);
    await screeningModel.insertMany([{ ...dissoc("theatreSize", value), seatsMap }]);
    res.status(200).send()
})

screeningRouter.put('/:id', async (req: Request, res: Response) => {
    const { id } = req.params
    let updateQuery: any = {}
    if (req.body.time)
        updateQuery.time = req.body.time

    if (req.body.isVip)
        updateQuery.isVip = req.body.isVip

    if (req.body.movieId)
        updateQuery.movieId = req.body.movieId

    await screeningModel.updateOne({ _id: id }, updateQuery)
    res.status(200).send()
})

screeningRouter.delete('/:id', async (req: Request, res: Response) => {
    const { id } = req.params
    await screeningModel.remove({ _id: id })
    res.status(200).send()
})

const THEATRE_SIZES = {
    s: {
        rows: 5,
        columns: 5
    },
    m: {
        rows: 10,
        columns: 10
    },
    l: {
        rows: 15,
        columns: 15
    },
}

const theatreSizeToSeatsMap = (size: "s" | "m" | "l"): boolean[][] => {
    const theatre = THEATRE_SIZES[size];
    return repeat(repeat(false, theatre.columns), theatre.rows);
}