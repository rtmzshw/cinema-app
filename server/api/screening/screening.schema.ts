import mongoose from "mongoose";
import Joi from 'joi';

const screening = new mongoose.Schema({
    //timestamp
    time: Number,
    movieId: String,
    isVip: Boolean,
    seatsMap: Array<Array<Boolean>>
});

export const screeningQueryJoiSchema = Joi.object({
    movieId: Joi.string().optional(),
    isVip: Joi.boolean().optional(),
    time: Joi.object({
        min: Joi.number().required(),
        max: Joi.number().required()
    }).optional(),
}).unknown()

export const postScreeningBodyJoiSchema = Joi.object({
    movieId: Joi.string().required(),
    isVip: Joi.boolean().required(),
    time: Joi.number().required(),
    theatreSize: Joi.string().valid("s","m","l").required()
}).unknown()

export const screeningModel = mongoose.model('screening', screening);