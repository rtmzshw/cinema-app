import { app, io } from "../.."
import { request, Request, Response, Router } from "express"
import { ordersModel, postOrderRequestBodyJoiSchema } from "./orders.schema"
import { screeningModel } from "../screening/screening.schema"

export const ordersRouter = Router()

ordersRouter.get('/', async (req: Request, res: Response) => {
    const allOrders = await ordersModel.find({});
    res.json(allOrders)
})

ordersRouter.get('/:userId', async (req: Request, res: Response) => {
    const allOrderForGivenUser = await ordersModel.find({ userId: req.params.userId })
    res.json(allOrderForGivenUser)
})

ordersRouter.post('/', async (req: Request, res: Response) => {
    const { error, value } = postOrderRequestBodyJoiSchema.validate(req.body);

    if (error) {
        console.log("validation error");
        console.log(JSON.stringify(error));
        return res.sendStatus(400);
    }

    const { userId, selectedSeats, updatedSeatsMap, screeningId } = value;
    await ordersModel.insertMany([{ userId, selectedSeats, screeningId, date: Date.now() }]);
    await screeningModel.findByIdAndUpdate(screeningId, { seatsMap: updatedSeatsMap });
    io.emit("sit_taken", { selectedSeats, screeningId });
    res.sendStatus(200);
})

ordersRouter.delete('/:id', async (req: Request, res: Response) => {
    const { id } = req.params
    const order = await ordersModel.findById(id);
    if (order) {
        const screening = await screeningModel.findById(order.screeningId);
        order.selectedSeats.map(({ row, column }) => {
            screening.seatsMap[row][column] = false;
        })
        await ordersModel.remove({ _id: id })
        await screeningModel.findByIdAndUpdate(order.screeningId, { seatsMap: screening.seatsMap });
        res.status(200).send();
    } else {
        console.log("order not found " + id);
        res.sendStatus(404);
    }
})