import Joi from "joi";
import mongoose from "mongoose"

const orders = new mongoose.Schema({
    userId: String,
    // the indexes of the chairs
    selectedSeats: Array<{ row: Number, column: Number }>,
    screeningId: String,
    date: Number
});

export const postOrderRequestBodyJoiSchema = Joi.object({
    userId: Joi.string().required(),
    screeningId: Joi.string().required(),
    selectedSeats: Joi.array().items(Joi.object({
        row: Joi.number().required(),
        column: Joi.number().required()
    })).required(),
    updatedSeatsMap: Joi.array().items(Joi.array().items(Joi.boolean())).required()
}).unknown()

export const ordersModel = mongoose.model('orders', orders);