import mongoose from "mongoose"

const users = new mongoose.Schema({
    // It will be the mail
    _id: String,
    firstName: String,
    lastName: String,
    password: String,
    // admin or client
    role: String
});

export const usersModel = mongoose.model('users', users);