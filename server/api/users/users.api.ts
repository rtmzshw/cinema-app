import { Request, Response, Router } from "express"
import { app } from "../.."
import { usersModel } from "./users.schema"

export const userRouter = Router()

userRouter.get('/', async (req: Request, res: Response) => {
    const users = await usersModel.find({});
    res.json(users)
})

userRouter.get('/:userId', async (req: Request, res: Response) => {
    const user = await usersModel.findById(req.params.userId);
    res.json(user)
})

userRouter.put('/:userId', async (req: Request, res: Response) => {
    const { userId } = req.params;
    const { firstName, lastName } = req.body;
    const user = await usersModel.findByIdAndUpdate(userId, { firstName, lastName });
    res.json(user)
})

userRouter.post('/', async (req: Request, res: Response) => {
    //TOOD: add fields if needed, i couldnt think of anything
    const { body } = req
    //validate req body
    await usersModel.insertMany([body])
    res.status(200).send()
})