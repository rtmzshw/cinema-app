import express, { Request, Response, NextFunction } from 'express'
export const app = express()
import cors from 'cors'
import http from 'http';
import { connectMongo } from './mongo'
import { scrapeMovies } from './scraper/scraper'
import { Server } from "socket.io";
import { userRouter } from './api/users/users.api';
import { screeningRouter } from './api/screening/screening.api';
import { ordersRouter } from './api/orders/orders.api';
import { moviesRouter } from './api/movies/movies.api';

const server = http.createServer(app);
const port = 4200
export const io = new Server(server, {
    cors: {
        origin: "http://localhost:3000",
        methods: ["GET", "POST"],
    },
});

const authMiddleware = (req: Request, res: Response, next: NextFunction) => {
    if (!req.headers['user_id']) {
        console.log("request without user");
        res.redirect("http://localhost:3000/login")
    } else {
        next();
    }
}

app.use(cors());
app.use(express.json())

app.use('/user/', userRouter)
app.use('/screening/', screeningRouter)
app.use('/order/', ordersRouter)
app.use('/movie/', moviesRouter)

io.on('connection', (socket) => {
    console.log(`User Connected: ${socket.id}`);
});

const start = async () => {
    console.log("connecting to mongo");
    await connectMongo();
    console.log("connnected to mongo");
    console.log("connecting to movie scarper");
    // await scrapeMovies()
    console.log("connnected to movie scarper");
    server.listen(port, () => {
        console.log(`Example app listening on port ${port}`)
    })
}

start()
