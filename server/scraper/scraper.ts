import { connection } from "mongoose";
import { moviesModel } from "../api/movies/movie.schema";

const jsdom = require("jsdom");
const { JSDOM } = jsdom;

const url = "https://www.imdb.com/chart/top/?ref_=nv_mv_250";

export const scrapeMovies = async () => {
    const dom = await JSDOM.fromURL(url)
    const { document } = dom.window;

    const table = document.querySelector("tbody")
    const movieDivs = table.querySelectorAll("tr");

    const movies = [];
    movieDivs.forEach(movieDiv => {
        const imageTd = movieDiv.children[0]
        const name = movieDiv.children[1].querySelector("a").innerHTML
        let pattern = /\(([^)]+)\)/
        const year = movieDiv.children[1].querySelector("span").innerHTML.match(pattern)[1]
        const rating = movieDiv.children[2].querySelector("strong").innerHTML
        const src = imageTd.querySelector("a").querySelector("img").src.replace(/@.*/, "@._V1_.jpg")
        movies.push({ src, name, year, rating, price: Math.ceil(Math.random() * (100 - 30) + 30) })
    });
    await moviesModel.insertMany(movies)
}
