import axios from "axios";
import { Screening } from "../Containers/App/App.types";


export interface ScreeningRequestQuery {
    movieId?: string;
    isVip?: boolean;
    time?: {
        min: number;
        max: number;
    };
}

export const getScreenings = async (q: ScreeningRequestQuery = {}) => {
    const { data: screenings } = await axios.get<Screening[]>('http://localhost:4200/screening', { params: { q } });
    return screenings;
};

export interface PostScreeningRequestBody extends ScreeningRequestQuery {
    // movieId: string;
    // isVip: boolean;
    // time: {
    //     min: number;
    //     max: number;
    // };
    theatreSize?: 's' | 'm' | 'l';
}

export const postScreening = async (body: PostScreeningRequestBody) => axios.post("http://localhost:4200/screening", { ...body, time: body.time?.min });

export const deleteScreening = async (id: string) => await axios.delete(`http://localhost:4200/screening/${id}`)