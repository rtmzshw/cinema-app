import axios from "axios"
import { Order, User } from "../Containers/App/App.types"

export const getUser = async (email: string) => {
    const { data: user } = await axios.get<User>(`http://localhost:4200/user/${email}`);
    return user;
}

export const addUser = async (user: User) => axios.post(`http://localhost:4200/user`, user);

export const updateUser = async (email: string, data: { lastName?: string, firstName?: string }) => axios.put(`http://localhost:4200/user/${email}`, data);