import axios from "axios";
import { prop, uniqBy } from "ramda";
import { Movie } from "../Containers/App/App.types";

export interface MovieRequestQuery {
    id?: string;
    name?: string;
    rank?: number;
    price?: { min: number; max: number; };
    byYear?: boolean;
}

export const getMovies = async (q: MovieRequestQuery = {}) => {
    const { data: movies } = await axios.get<Movie[]>('http://localhost:4200/movie', { params: { q } });
    const movieUniqByName = uniqBy(prop("name"), movies);
    return movieUniqByName;
};

export const getMovie = async (id: string) => {
    const { data: movie } = await axios.get<Movie>(`http://localhost:4200/movie/byId/${id}`);
    return movie;
};

export const postMovie = async (body: Omit<Movie, "_id">) => await axios.post('http://localhost:4200/movie', body);

export const deleteMovie = async (id: string) => await axios.delete(`http://localhost:4200/movie/${id}`);