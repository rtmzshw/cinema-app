import axios from "axios";
import { Order } from "../Containers/App/App.types";

export interface SendOrderRequest {
    userId: string;
    screeningId: string;
    selectedSeats: { row: number; column: number; }[];
    updatedSeatsMap: boolean[][];
}

export const sendOrder = async (body: SendOrderRequest) => axios.post("http://localhost:4200/order", body);

export const getAllOrders = async () => {
    const { data: orders } = await axios.get<Order[]>(`http://localhost:4200/order`);
    return orders;
};

export const cancelOrder = async (orderId: string) => axios.delete(`http://localhost:4200/order/${orderId}`);

export const getUserOrderHistory = async (userId: string) => {
    const { data: orders } = await axios.get<Order[]>(`http://localhost:4200/order/${userId}`);
    return orders;
}