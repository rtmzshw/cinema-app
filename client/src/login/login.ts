import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut as signOutFromFirebase } from "firebase/auth";
import { addUser, getUser } from "../api/user";
import { User } from "../Containers/App/App.types";
import { firebaseConf } from '../firebase.config'

export const register = async (user: User) => {
    const auth = getAuth(firebaseConf);
    const { user: userFromFirebase } = await createUserWithEmailAndPassword(auth, user._id, user.password)
    await addUser(user);
    return user
}

export const login = async (email: string, password: string) => {
    const auth = getAuth(firebaseConf);
    const { user } = await signInWithEmailAndPassword(auth, email, password);
    const userFromDb = await getUser(email);
    return userFromDb;
}

export const signOut = () => {
    const auth = getAuth(firebaseConf);
    return signOutFromFirebase(auth)
}