// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyD6b0b9ImpT4Xn247DmSHQ7LG1AmufdijI",
  authDomain: "cinema-26ce7.firebaseapp.com",
  projectId: "cinema-26ce7",
  storageBucket: "cinema-26ce7.appspot.com",
  messagingSenderId: "262494787847",
  appId: "1:262494787847:web:56b117169fc3f155030da2"
};

// Initialize Firebase
export const firebaseConf = initializeApp(firebaseConfig);