import { MenuItem, Select, TextField, withStyles, WithStyles } from "@material-ui/core";
import moment from "moment";
import { dissoc } from "ramda";
import React, { ChangeEvent, FunctionComponent, useState } from "react";
import { ScreeningRequestQuery } from "../../api/screening";
import { Movie } from "../../Containers/App/App.types";
import { calcDayRange, isPastDate } from "../../utils";
import InputCell from "../InputCell/InputCell";
import { styles } from "./FindScreening.styles";

export interface FiltersProps extends WithStyles<typeof styles> {
    movies: Movie[];
    getScreeningsQuery: ScreeningRequestQuery;
    setGetScreeningsQuery: React.Dispatch<React.SetStateAction<ScreeningRequestQuery>>;
    actionButton: JSX.Element;
    action: "post" | "get";
    style?: React.CSSProperties;
}

const Filters: FunctionComponent<FiltersProps> = props => {
    const { classes, style, movies, getScreeningsQuery, setGetScreeningsQuery, actionButton, action } = props;
    const [selectedMovie, setMovie] = useState<Movie | null>(null);
    const [isVip, setIsVip] = useState<"vip" | "regular" | "all">(action === "get" ? "all" : "regular");

    const onDateChosen = (e: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
        if (action === "get") {
            const chosenDate = new Date(e.target.value);
            const { min, max } = calcDayRange(chosenDate);
            setGetScreeningsQuery({ ...getScreeningsQuery, time: { min, max } });
        } else if (action === "post") {
            const { target: { value } } = e;
            const time = (new Date(value)).getTime();
            setGetScreeningsQuery({ ...getScreeningsQuery, time: { min: time, max: time } });
        }

    }

    const setScreeningType = (e: React.ChangeEvent<{ name?: string, value: unknown }>) => {
        setIsVip(e.target.value as any);
        if (e.target.value === "vip") {
            setGetScreeningsQuery({ ...getScreeningsQuery, isVip: true });
        } else if (e.target.value === "regular") {
            setGetScreeningsQuery({ ...getScreeningsQuery, isVip: false });
        } else {
            setGetScreeningsQuery(dissoc("isVip", getScreeningsQuery));
        }
    }

    return (
        <div className={classes.filters} style={style}>
            {actionButton}
            <InputCell className={classes.inputCell} title="סוג הקרנה">
                <Select
                    className={classes.select}
                    style={{ width: '100%' }}
                    value={isVip}
                    variant="outlined"
                    onChange={setScreeningType}
                >
                    {action === "get" && <MenuItem key={"all"} value={"all"}>הכל</MenuItem>}
                    <MenuItem key={"regular"} value={"regular"}>רגילה</MenuItem>
                    <MenuItem key={"vip"} value={"vip"}>VIP</MenuItem>
                </Select>
            </InputCell>
            <InputCell title="תאריך">
                <TextField
                    style={{ colorScheme: "dark" }}
                    type={action === "post" ? "datetime-local" : "date"}
                    variant="outlined"
                    value={moment(getScreeningsQuery.time?.min).format(action === "post" ? "yyyy-MM-DDThh:mm" : "yyyy-MM-DD")}
                    onChange={onDateChosen}
                    error={isPastDate(getScreeningsQuery)}
                    label={isPastDate(getScreeningsQuery) ? "תאריך לא תקין" : ""}
                />
            </InputCell>
            <InputCell className={classes.inputCell} title="בחר סרט">
                <Select
                    className={classes.select}
                    value={selectedMovie?.name ?? ""}
                    variant="outlined"
                    onChange={event => {
                        const newMovie = movies.find(({ name }) => name === event.target.value);
                        if (newMovie) {
                            setMovie(newMovie);
                            setGetScreeningsQuery({ ...getScreeningsQuery, movieId: newMovie._id });
                        }
                    }}
                >
                    {movies.map(({ name }) => {
                        return (
                            <MenuItem key={name} value={name}>{name}</MenuItem>
                        )
                    })}
                </Select>
            </InputCell>
        </div>
    )
}

export default withStyles(styles)(Filters);