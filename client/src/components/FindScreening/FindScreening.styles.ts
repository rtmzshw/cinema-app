import { createStyles, Theme } from "@material-ui/core";

export const styles = (theme: Theme) =>
    createStyles({
        root: {
            width: '100%',
            height: '30%',
            display: 'flex',
            flexDirection: 'column',
            // justifyContent: 'center',
            alignItems: 'end'
        },
        filters: {
            width: '55%',
            padding: '10px',
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
        },
        inputCell: {
            width: '25%'
        },
        select: {
            width: '100%'
        },
        searchBtn: {
            fontFamily: 'inherit',
            width: '10%',
            alignSelf: 'flex-end'
        }
    })