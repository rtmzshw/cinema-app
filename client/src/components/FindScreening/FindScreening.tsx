import DateFnsUtils from "@date-io/date-fns";
import { Button, MenuItem, Select, withStyles, WithStyles } from "@material-ui/core";
import { KeyboardDatePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import { MaterialUiPickersDate } from "@material-ui/pickers/typings/date";
import moment from "moment";
import { dissoc, isEmpty } from "ramda";
import React, { FunctionComponent, useState } from "react";
import { ScreeningRequestQuery } from "../../api/screening";
import { Movie } from "../../Containers/App/App.types";
import { calcDayRange, isPastDate } from "../../utils";
import InputCell from "../InputCell/InputCell";
import SubTitle from "../SubTitle/SubTitle";
import Filters from "./Filters";
import { styles } from "./FindScreening.styles";

export interface FindScreeningProps extends WithStyles<typeof styles> {
    movies: Movie[];
    getFilteredScreenings: (q: ScreeningRequestQuery) => void;
}

const FindScreening: FunctionComponent<FindScreeningProps> = props => {
    const { classes, movies, getFilteredScreenings } = props;
    const [getScreeningsQuery, setGetScreeningsQuery] = useState<ScreeningRequestQuery>({ time: calcDayRange(new Date()) });

    const findScreeningsBtn = <Button onClick={e => getFilteredScreenings(getScreeningsQuery)} disabled={isEmpty(getScreeningsQuery) || isPastDate(getScreeningsQuery)} className={classes.searchBtn} variant="contained" color="primary">חפש</Button>

    return (
        <div className={classes.root}>
            <SubTitle title="הזמנת כרטיסים" />
            <Filters {...{ getScreeningsQuery, setGetScreeningsQuery, movies, actionButton: findScreeningsBtn, action: "get" }} />
        </div>
    )
}

export default withStyles(styles)(FindScreening);