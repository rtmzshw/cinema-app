import { Grid, makeStyles } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { User } from "../../Containers/App/App.types";
import { register } from "../../login/login";
import RtlTextField from "../RtlTextField/RtlTextField";

const useStyles = makeStyles((theme) => ({
  container: {
    textAlign: "center",
    marginBottom: "10px",
    cursor: "pointer",
    direction: "rtl",
    backgroundColor: "white",
    padding: "15px",
    margin: "10px",
  },
  item: {
    display: "flex",
    width: "300px",
  },
  itemData: {
    width: "100px",
    marginLeft: "20px",
    marginTop: "10px",
  },
  image: {
    height: "100px",
    width: "100px",
    marginBottom: "15px",
  },
  iconButton: {
    marginRight: theme.spacing(2),
    marginTop: "30px",
  },
  label: {
    left: "unset",
    right: 0
  },
  link: {
    textDecoration: "none",
    color: "unset"
}
}));

const Register = (props: { setUser: React.Dispatch<React.SetStateAction<User>> }) => {
  const classes = useStyles();
  const [userData, setUserData] = useState<User>({ _id: "", firstName: "", lastName: "", password: "", role: "client" });

  const registerUser = async () => {
    try {
      await register(userData);
      props.setUser(userData);
      Swal.fire("נרשמת בהצלחה למערכת!");
    } catch (e) {
      alert(e);
    }
  }

  return (
    <Grid
      container
      direction="column"
      alignContent="center"
      alignItems="center"
      justifyContent="space-around"
      style={{ height: "50%" }}
    >
      <RtlTextField value={userData.firstName} label="שם פרטי" onChange={e => setUserData({ ...userData, firstName: e.target.value })} />
      <RtlTextField value={userData.lastName} label="שם משפחה" onChange={e => setUserData({ ...userData, lastName: e.target.value })} />
      <RtlTextField value={userData._id} label="אי-מייל" onChange={e => setUserData({ ...userData, _id: e.target.value })} />
      <RtlTextField value={userData.password} label="סיסמא" onChange={e => setUserData({ ...userData, password: e.target.value })} type="password" />
      <Link className={classes.link} to={'/'}><Button variant="contained" color="primary" onClick={e => registerUser()}>הרשם</Button></Link>
    </Grid>
  );
};

export default Register;
