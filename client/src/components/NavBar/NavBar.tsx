import { AppBar, Button, Toolbar, withStyles, WithStyles } from "@material-ui/core";
import React, { FunctionComponent, useEffect } from "react";
import { User } from "../../Containers/App/App.types";
import { styles } from "./NavBar.styles";
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import IconButton from "@material-ui/core/IconButton";
import { signOut } from "../../login/login";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import { Link } from "react-router-dom";

export interface NavBarProps extends WithStyles<typeof styles> {
    user: User;
    setUser: React.Dispatch<React.SetStateAction<User>>;
}

const NavBar: FunctionComponent<NavBarProps> = props => {
    const { user, setUser, classes } = props;

    const logout = async () => {
        setUser({ _id: "", firstName: "", lastName: "", password: "", role: "client" });
        await signOut();
    }

    const Logo = <Link to={"/"}><img alt="logo" style={{ height: "9vh" }} src={require('./planetCinemaLogo.png')} /></Link>

    return (
        <AppBar position="static">
            {(user && user._id !== "") ?
                <Toolbar className={classes.titleBar}>
                    <Link className={classes.link} to={"/login"}>
                        <Button color="inherit" onClick={logout}>
                            <ExitToAppIcon style={{ marginRight: "5px" }} fontSize="large" />
                            Log out
                        </Button>
                    </Link>
                    {Logo}
                    <Link className={classes.link} to={"/profile"}>
                        <IconButton>
                            <AccountCircleIcon fontSize="large" />
                        </IconButton>
                    </Link>
                </Toolbar>
                :
                <Toolbar className={classes.titleBar}>
                    <Link className={classes.link} to={"/login"}>
                        <Button color="inherit">Login</Button>
                    </Link>
                    {Logo}
                    <Link className={classes.link} to={"/register"}>
                        <Button color="inherit">Register</Button>
                    </Link>
                </Toolbar>
            }
        </AppBar>
    )
}

export default withStyles(styles)(NavBar);