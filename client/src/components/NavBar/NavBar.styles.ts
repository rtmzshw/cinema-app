import { createStyles, Theme } from "@material-ui/core";

export const styles = (theme: Theme) =>
    createStyles({
        titleBar: {
            display: 'flex',
            backgroundColor: 'black',
            justifyContent: 'space-between',
            alignItems: 'center',
            height: "10vh"
        },
        secondBar: {
            backgroundColor: '#0e0e0e',
        },
        link: {
            textDecoration: "none",
            color: "unset"
        }
    })