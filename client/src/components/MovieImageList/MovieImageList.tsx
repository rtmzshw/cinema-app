import { ImageList, ImageListItem, ImageListItemBar, withStyles, WithStyles } from "@material-ui/core";
import StarBorderIcon from '@material-ui/icons/StarBorder';
import React, { FunctionComponent } from "react";
import { Movie } from "../../Containers/App/App.types";
import { styles } from "./MovieImageList.styles";

export interface MovieImageListProps extends WithStyles<typeof styles> {
    movies: Movie[];
}

const MovieImageList: FunctionComponent<MovieImageListProps> = props => {
    const { movies, classes } = props;
    return (
        <div className={classes.root}>
            <ImageList className={classes.imageList} cols={6}>
                {movies.map(({ src, name, rating }) => (
                    <ImageListItem key={`${src}${name}`} style={{ height: '100%' }}>
                        <img src={src} alt={name} />
                        <ImageListItemBar
                            title={name}
                            classes={{
                                root: classes.titleBar,
                                title: classes.title,
                            }}
                            actionIcon={
                                <span style={{ display: "flex", alignItems: "center" }}>
                                    {rating}
                                    <StarBorderIcon color="secondary" className={classes.title} style={{ marginBottom: "3px", marginRight: "3px" }} />
                                </span>
                            }
                        />
                    </ImageListItem>
                ))}
            </ImageList>
        </div>
    )
}

export default withStyles(styles)(MovieImageList);