import { createStyles, Theme } from "@material-ui/core";

export const styles = (theme: Theme) =>
    createStyles({
        dialogPaper: {
            minHeight: "70vh",
            minWidth: "40vw"
        },
        dialogContent: {
            padding: "0 25px",
            maxHeight: "50vh",
            overflowY: "auto"
        },
        seatIcon: {
            color: theme.palette.warning.main,
            cursor: "pointer"
        },
        rowNum: {
            width: "5%",
            textAlign: "left"
        },
        seatsIcons: {
            display: "flex",
            justifyContent: "center",
            width: "95%"
        },
        takenSeatIcon: {
            color: "#7e8387"
        },
        chosenSeatIcon: {
            color: theme.palette.success.main,
            cursor: "pointer"
        },
        btn: {
            background: 'none',
            border: 'none',
        },
        seatsRow: {
            display: "flex",
            alignItems: "center"
        },
        warningDiv: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-end',
            fontSize: 'small'
        },
        errorIcon: {
            padding: '0 5px',
            fontSize: '25px'
        },
        continueBtn: {
            position: "absolute", 
            bottom: 0, 
            padding: "15px"
        }
    })