import { Dialog, DialogTitle, withStyles, WithStyles } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import ErrorIcon from '@material-ui/icons/Error';
import EventSeatIcon from '@material-ui/icons/EventSeat';
import { isEmpty, uniq } from "ramda";
import React, { FunctionComponent, useEffect, useState } from "react";
import Swal from "sweetalert2";
import { Screening, Seat } from "../../Containers/App/App.types";
import { copyArr } from "../../utils";
import { styles } from "./SeatSelection.styles";

export interface SeatSelectionProps extends WithStyles<typeof styles> {
    screening: Screening;
    dialogOpenState: boolean;
    closeDialog: () => void;
    sendOrder: (selectedSeats: Seat[], updatedSeatsMap: boolean[][]) => void;
    isUserLogged: boolean;
}

const SeatSelection: FunctionComponent<SeatSelectionProps> = props => {
    const { screening: { seatsMap }, dialogOpenState, closeDialog, sendOrder, isUserLogged, classes } = props;
    const [tempSeatsMap, setTempSeatsMap] = useState<boolean[][]>(copyArr(seatsMap));
    const [selectedSeats, setSelectedSeats] = useState<Seat[]>([]);

    useEffect(() => {
        setTempSeatsMap(copyArr(seatsMap));
    }, [seatsMap])

    //true => sold seat
    const getSeatStatus = (row: number, seat: number): "sold" | "selected" | "available" =>
        seatsMap[row][seat] ? "sold" :
            tempSeatsMap[row][seat] ? "selected" :
                "available"

    const seatStatusToClassName = {
        "sold": classes.takenSeatIcon,
        "selected": classes.chosenSeatIcon,
        "available": classes.seatIcon
    }

    const checkSeatSelectionValidity = (selectedSeats: Seat[]) => {
        if (isEmpty(selectedSeats)) {
            return false;
        }
        return selectedSeats.map(({ row, column }) => {
            if (column - 1 >= 0 && !tempSeatsMap[row][column - 1]) {
                if (column - 1 === 0) {
                    return false;
                }
                if (tempSeatsMap[row][column - 2]) {
                    return false;
                }
            }
            if (column + 1 < tempSeatsMap[row].length && !tempSeatsMap[row][column + 1]) {
                if (column + 1 === tempSeatsMap[row].length - 1) {
                    return false;
                }
                if (tempSeatsMap[row][column + 2]) {
                    return false;
                }
            }
            return true;
        }).every(Boolean)
    }

    const updateSeat = (row: number, seat: number, status: boolean) => {
        const newTempSeatsMap = copyArr(tempSeatsMap);
        newTempSeatsMap[row][seat] = status;
        setTempSeatsMap(newTempSeatsMap);
    }
    
    const selectSeat = (row: number, seat: number) => {
        updateSeat(row, seat, true);
        setSelectedSeats(selectedSeats.concat({ row, column: seat }));
    }
    
    const unselectSeat = (row: number, seat: number) => {
        updateSeat(row, seat, false);
        const updatedSelectedSeats = selectedSeats.filter(s => !(s.row === row && s.column === seat));
        setSelectedSeats(updatedSelectedSeats);
    }

    const createSeatsRow = (rowNumber: number, seats: boolean[]) =>
        seats.map((seat, seatNumber) => {
            const seatStatus = getSeatStatus(rowNumber, seatNumber);
            return (
                <button
                    className={classes.btn}
                    key={`${rowNumber},${seatNumber}`}
                    disabled={seatStatus === "sold"}
                    onClick={e => seatStatus === "available" ? selectSeat(rowNumber, seatNumber) : unselectSeat(rowNumber, seatNumber)}
                >
                    <EventSeatIcon
                        className={seatStatusToClassName[seatStatus]}
                    />
                </button>
            )
        })

    const onContinue = (e: any) => {
        if (isUserLogged) {
            sendOrder(uniq(selectedSeats), tempSeatsMap);
            Swal.fire("ההזמנה בוצעה בהצלחה");
        } else {
            alert("נדרש להתחבר לפני ביצוע ההזמנה");
        }
    }

    return (
        <Dialog onClose={closeDialog} open={dialogOpenState} classes={{ paper: classes.dialogPaper }}>
            <DialogTitle style={{ direction: "rtl" }}>בחירת מושבים</DialogTitle>
            <div className={classes.dialogContent}>
                <div>
                    {tempSeatsMap.map((seats, rowNumber) =>
                        <div key={rowNumber} className={classes.seatsRow}>
                            <span className={classes.rowNum}>{rowNumber + 1}</span>
                            <span className={classes.seatsIcons}>{createSeatsRow(rowNumber, seats)}</span>
                        </div>
                    )}
                </div>
            </div>
            <div className={classes.warningDiv}>
                לא ניתן להשאיר כסא פנוי בודד
                <ErrorIcon className={classes.errorIcon} color="error" />
            </div>
            <div className={classes.continueBtn}>
                <Button
                    variant="contained"
                    color="primary"
                    disabled={!checkSeatSelectionValidity(selectedSeats)}
                    onClick={onContinue}
                >
                    המשך
                </Button>
            </div>
        </Dialog >
    )
}

export default withStyles(styles)(SeatSelection);