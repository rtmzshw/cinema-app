import { makeStyles, TextField } from "@material-ui/core";
import { TextFieldProps } from "@material-ui/core/TextField";
import React from "react";

const useStyles = makeStyles((theme) => ({
    label: {
        left: "unset",
        right: 0
    },
    text: {
        direction: "rtl"
    }
}));

const RtlTextField = (props: TextFieldProps) => {
    const classes = useStyles();

    return (
        <TextField {...props}
            InputLabelProps={{ classes: { formControl: classes.label } }}
            InputProps={{ classes: { input: classes.text } }} />
    );
};

export default RtlTextField;
