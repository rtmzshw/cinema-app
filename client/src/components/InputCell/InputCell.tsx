import React, { FunctionComponent } from "react";

export interface InputCellProps {
    title: string;
    className?: string;
    style?: React.CSSProperties;
}

const InputCell: FunctionComponent<InputCellProps> = props => {
    return (
        <div className={props.className} style={props.style}>
            <h4 style={{
                textAlign: 'end',
                color: 'white',
                margin: '0 0 7px 0'
            }}>
                {props.title}
            </h4>
            {props.children}
        </div>

    )
}

export default (InputCell);