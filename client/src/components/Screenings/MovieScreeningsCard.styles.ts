import { createStyles, Theme } from "@material-ui/core";

export const styles = (theme: Theme) =>
    createStyles({
        root: { 
            display: "flex", 
            justifyContent: "flex-end", 
            height: "40%",
            color: "white",
            width: "30%",
            marginLeft: "auto"
        },
        screenings: {
            display: "flex", 
            flexDirection: "column", 
            margin: "10px"
        },
        content: {
            direction: "rtl"
        },
        screeningTimes: { 
            display: "flex", 
            alignItems: "center", 
            justifyContent: "flex-start"
        },
        time: {
            padding: "10px",
            cursor: "pointer",
            fontSize: "large",
            fontWeight: 700
        },
        screeningType: {
            margin: 0
        },
        movieImg: {
            height: "95%",
            padding: "10px"
        },
        paper: {
            backgroundColor: theme.palette.warning.main,
            boxShadow: "none",
            margin: "5px",
            padding: "5px"
        }
    })