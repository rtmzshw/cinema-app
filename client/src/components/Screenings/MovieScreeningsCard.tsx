import { withStyles, WithStyles } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import moment from "moment";
import { groupBy, isEmpty, isNil, toPairs } from "ramda";
import React, { FunctionComponent, useEffect, useState } from "react";
import { v4 as uuid } from "uuid";
import { getMovie as getMovieFromServer } from "../../api/movie";
import { Movie, Screening } from "../../Containers/App/App.types";
import SubTitle from "../SubTitle/SubTitle";
import { styles } from "./MovieScreeningsCard.styles";

export interface MovieScreeningsCardProps extends WithStyles<typeof styles> {
    screenings: Screening[];
    movieId: string;
    openSeatSelectionDialog: (screening: Screening) => void;
}

const isNilOrEmpty = (any: any): boolean => isNil(any) || isEmpty(any)

const MovieScreeningsCard: FunctionComponent<MovieScreeningsCardProps> = props => {
    const { screenings, movieId, openSeatSelectionDialog, classes } = props;
    const [movie, setMovie] = useState<Movie>({ _id: "", name: "", price: 0, rating: 0, src: "", year: 0 });
    const { regular: regularScreenings, vip: vipScreenings } = groupBy(({ isVip }) => isVip ? "vip" : "regular", screenings);

    useEffect(() => {
        const getMovie = async () => {
            const movie = await getMovieFromServer(movieId);
            setMovie(movie);
        }

        getMovie();
    }, [movieId])

    const createScreeningRow = (screenings: Screening[], screeningType: string) => {
        const screeningsByDate = toPairs(groupBy(({ time }) => moment(time).format("DD/MM"), screenings));
        return (
            <div key={screeningType}>
                <h4 className={classes.screeningType}>{screeningType}</h4>
                <div className={classes.screeningTimes}>
                    {screeningsByDate.map(([date, screenings]) =>
                        <Paper key={`${screeningType}${date}${screenings[0].movieId}`} className={classes.paper}>
                            <span className={classes.time} >{date}</span>
                            <br />
                            {screenings.map(screening =>
                                <span key={uuid()} className={classes.time} onClick={e => openSeatSelectionDialog(screening)}>
                                    {moment(screening.time).format("HH:mm")}
                                </span>
                            )}
                        </Paper>
                    )}
                </div>
            </div>
        )
    }

    return (
        <div className={classes.root}>
            <div className={classes.content}>
                <SubTitle title={movie.name} style={{ textAlign: "start" }} />
                <div className={classes.screenings}>
                    {!isNilOrEmpty(vipScreenings) && createScreeningRow(vipScreenings, 'VIP')}
                    {!isNilOrEmpty(regularScreenings) && createScreeningRow(regularScreenings, '2D')}
                </div>
            </div>
            <img src={movie.src} alt={movie.name} className={classes.movieImg} />
        </div>
    )
}

export default withStyles(styles)(MovieScreeningsCard);