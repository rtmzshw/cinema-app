import { Grid, makeStyles } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { getUser } from "../../api/user";
import { User } from "../../Containers/App/App.types";
import { login } from "../../login/login";
import RtlTextField from "../RtlTextField/RtlTextField";

const useStyles = makeStyles((theme) => ({
  container: {
    textAlign: "center",
    marginBottom: "10px",
    cursor: "pointer",
    direction: "rtl",
    backgroundColor: "white",
    padding: "15px",
    margin: "10px",
  },
  item: {
    display: "flex",
    width: "300px",
  },
  itemData: {
    width: "100px",
    marginLeft: "20px",
    marginTop: "10px",
  },
  image: {
    height: "100px",
    width: "100px",
    marginBottom: "15px",
  },
  iconButton: {
    marginRight: theme.spacing(2),
    marginTop: "30px",
  },
  link: {
    textDecoration: "none",
    color: "unset"
  }
}));

const Login = (props: { setUser: React.Dispatch<React.SetStateAction<User>> }) => {
  const classes = useStyles();
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  const loginUser = async () => {
    try {
      const user = await login(email, password);
      props.setUser(user);
      Swal.fire("התחברת בהצלחה למערכת!");
    } catch (e) {
      alert(e);
    }
  }

  return (
    <Grid
      container
      direction="column"
      alignContent="center"
      alignItems="center"
      justifyContent="space-around"
      style={{ height: "50%" }}
    >
      <RtlTextField label="אי-מייל" onChange={e => setEmail(e.target.value)} />
      <RtlTextField label="סיסמא" onChange={e => setPassword(e.target.value)} type="password" />
      <Link className={classes.link} to={"/"}><Button variant="contained" color="primary" onClick={e => loginUser()}>התחבר</Button></Link>
    </Grid>
  );
};

export default Login;
