import React, { FunctionComponent } from "react";

export interface SubTitleProps {
    title: string;
    style?: React.CSSProperties;
    chilren?: JSX.Element
}

const SubTitle: FunctionComponent<SubTitleProps> = props => {
    return (
        <h3 style={{
            textAlign: 'end',
            color: 'white',
            margin: '10px',
            ...props.style
        }}>
            {props.children}
            {props.title}
        </h3 >

    )
}

export default (SubTitle);