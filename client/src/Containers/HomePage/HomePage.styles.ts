import { createStyles, Theme } from "@material-ui/core";

export const styles = (theme: Theme) =>
    createStyles({
        root: {
            height: '100%',
            width: '100%'
        },
        movieImages: {
            height: '55%',
            width: '100%'
        },
        title: {
            textAlign: 'end',
            color: 'white',
            margin: '10px'
        }
    })