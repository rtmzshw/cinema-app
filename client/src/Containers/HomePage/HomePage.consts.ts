// import { Movie, Screening } from "../App/App.types"

// export const MOVIE: Movie = {
//     _id: "63a7504990e8c05a61ad1175",
//     name: "Good Will Hunting",
//     src: "https://m.media-amazon.com/images/M/MV5BOTI0MzcxMTYtZDVkMy00NjY1LTgyMTYtZmUxN2M3NmQ2NWJhXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_.jpg",
//     year: 1997,
//     rating: 8.3,
//     price: 62
// }

// export const SEATS = [
//     [
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true
//     ],
//     [
//         false,
//         false,
//         false,
//         false,
//         false,
//         false,
//         false,
//         false,
//         false
//     ],
//     [
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true
//     ],
//     [
//         false,
//         false,
//         false,
//         false,
//         false,
//         false,
//         false,
//         false,
//         false
//     ],
//     [
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true
//     ],
//     [
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true
//     ],
//     [
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true
//     ],
//     [
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true,
//         true
//     ]
// ]

// export const SCREENINGS: Screening[] = [
//     {
//         isVip: true,
//         movieId: "63a7504990e8c05a61ad1175",
//         seatsMap: SEATS,
//         time: Date.now()
//     },
//     {
//         isVip: true,
//         movieId: "63a7504990e8c05a61ad1175",
//         seatsMap: SEATS,
//         time: Date.now()
//     },
//     {
//         isVip: true,
//         movieId: "63a7504990e8c05a61ad1175",
//         seatsMap: SEATS,
//         time: Date.now() + 100000
//     },
//     {
//         isVip: false,
//         movieId: "63a7504990e8c05a61ad1175",
//         seatsMap: SEATS,
//         time: Date.now() + 20000
//     },
//     {
//         isVip: false,
//         movieId: "63a7504990e8c05a61ad1175",
//         seatsMap: SEATS,
//         time: Date.now() + 2345
//     },
//     {
//         isVip: false,
//         movieId: "63a7504990e8c05a61ad1175",
//         seatsMap: SEATS,
//         time: Date.now()
//     },
// ]

// export const A: [string, Screening[]][] = [
//     ["63a7504990e8c05a61ad1167", SCREENINGS.map(s=> ({...s, movieId: "63a7504990e8c05a61ad1167"}))],
//     [MOVIE._id, SCREENINGS]
// ]