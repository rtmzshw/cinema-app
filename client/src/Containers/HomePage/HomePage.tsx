import { withStyles, WithStyles } from "@material-ui/core";
import { groupBy, isEmpty, isNil, prop, toPairs } from "ramda";
import React, { FunctionComponent, useEffect, useState } from "react";
import { io } from "socket.io-client";
import FindScreening from "../../components/FindScreening/FindScreening";
import MovieImageList from "../../components/MovieImageList/MovieImageList";
import MovieScreeningsCard from "../../components/Screenings/MovieScreeningsCard";
import SeatSelection from "../../components/SeatSelection/SeatSelection";
import SubTitle from "../../components/SubTitle/SubTitle";
import { copyArr } from "../../utils";
import { sendOrder } from "../../api/order";
import { getScreenings, ScreeningRequestQuery } from "../../api/screening";
import { getMovies } from "../../api/movie";
import { Movie, Screening, Seat, User } from "../App/App.types";
import { styles } from "./HomePage.styles";

export interface HomePageProps extends WithStyles<typeof styles> {
    user: User;
    movies: Movie[];
}

const socket = io("http://localhost:4200");

const HomePage: FunctionComponent<HomePageProps> = props => {
    const { movies, user, classes } = props;
    const [filteredScreeningsByMovieId, setFilteredScreeningsByMovieId] = useState<[string, Screening[]][]>([["", []]]);
    const [dialogState, setDialogState] = useState<{ open: boolean; value: Screening }>({ open: false, value: { _id: "", isVip: false, movieId: "", seatsMap: [[]], time: 0 } })

    const openSeatSelectionDialog = (screening: Screening) => {
        setDialogState({ open: true, value: screening });
    }

    const featuringNowMovies = movies.slice(0, 10);

    const getFilteredScreenings = async (q: ScreeningRequestQuery) => {
        const screenings = await getScreenings(q);
        const screeningsByMovieId = toPairs(groupBy(prop("movieId"), screenings));
        setFilteredScreeningsByMovieId(screeningsByMovieId);
    }

    const onSendOrder = async (selectedSeats: Seat[], updatedSeatsMap: boolean[][]) => {
        await sendOrder({
            userId: user._id,
            screeningId: dialogState.value._id,
            selectedSeats,
            updatedSeatsMap
        });
    }

    socket.on("sit_taken", ({ selectedSeats, screeningId }) => {
        if (screeningId === dialogState.value._id) {
            const newSeatsMap = copyArr(dialogState.value.seatsMap);
            selectedSeats.map(({ row, column }: Seat) => {
                newSeatsMap[row][column] = true;
            })
            setDialogState({ ...dialogState, value: { ...dialogState.value, seatsMap: newSeatsMap } });
        }
    });

    const a = () => {
        console.log(JSON.stringify(filteredScreeningsByMovieId));
        const b = !isNil(filteredScreeningsByMovieId);
        console.log(b);
        const c = !isEmpty(filteredScreeningsByMovieId);
        console.log(c);
        const d = (b && c) ? filteredScreeningsByMovieId[0][0] !== "" : false;
        console.log(d);
        return d;
    }

    return (
        <div className={classes.root}>
            <SubTitle title="עכשיו בקולנוע" />
            <div className={classes.movieImages}>
                <MovieImageList movies={featuringNowMovies} />
            </div>
            <FindScreening {...{ movies, getFilteredScreenings }} />
            {a() && filteredScreeningsByMovieId.map(([movieId, screenings]) =>
                <MovieScreeningsCard key={movieId} {...{ movieId, screenings, openSeatSelectionDialog }} />
            )}
            <SeatSelection
                dialogOpenState={dialogState.open}
                closeDialog={() => setDialogState({ ...dialogState, open: false })}
                screening={dialogState.value}
                sendOrder={onSendOrder}
                isUserLogged={user._id !== ""}
            />
        </div >
    )
};

export default withStyles(styles)(HomePage);