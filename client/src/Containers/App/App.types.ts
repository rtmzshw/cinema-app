export interface Movie {
    _id: string;
    name: string;
    src: string;
    year: number;
    rating: number;
    price: number;
}

export const MOVIE_KEYS = ["name", "src", "year", "rating", "price"] as const

export interface Screening {
    _id: string;
    //timestamp
    time: number,
    movieId: string,
    isVip: boolean,
    // true -> seat sold
    seatsMap: boolean[][]
}

export type ScreeningType = "regular" | "vip"

export interface Seat {
    row: number;
    column: number;
}

export interface User {
    // email
    _id: string;
    firstName: string;
    lastName: string;
    password: string;
    // admin or client
    role: "admin" | "client";
}

export interface Order {
    _id: string;
    userId: string;
    screeningId: string;
    selectedSeats: Seat[];
    date: number;
}