import { createTheme, ThemeProvider, withStyles, WithStyles } from '@material-ui/core';
import React, { FunctionComponent, useEffect, useState } from 'react';
import { redirect, RouterProvider } from "react-router-dom";
import { getMovies } from '../../api/movie';
import Login from '../../components/Login/Login';
import NavBar from '../../components/NavBar/NavBar';
import './App.css';
import { createViews } from "./App.router";
import { styles } from './App.styles';
import { Movie, User } from './App.types';

interface AppProps extends WithStyles<typeof styles> { }

const App: FunctionComponent<AppProps> = (props) => {
  const { classes } = props;
  const [user, setUser] = useState<User>({ _id: "", firstName: "", lastName: "", password: "", role: "client" });
  const [movies, setMovies] = useState<Movie[]>([]);

  useEffect(() => {
    const getAllMovies = async () => {
      const newMovies = await getMovies();
      setMovies(newMovies);
    };

    getAllMovies();
  }, []);

  const router = createViews(user, setUser, movies);

  useEffect(() => {
    
    if (user._id === "") {
      console.log("user._id");
      console.log(user._id);
      redirect("http://localhost:3000/login");
      // window.location.replace("http://localhost:3000/login");
      // window.location.href = 'login'
    }
  }, [user._id])

  const darkTheme = createTheme({
    palette: {
      type: "dark",
      primary: {
        main: "#2e3d7c",
      },
      secondary: {
        main: "#ffa73c",
      },
      error: {
        main: "#ba292e",
      },
      warning: {
        main: "#ffa73c",
      },
    },
  });

  return (
    <ThemeProvider theme={darkTheme}>
      <RouterProvider router={router} fallbackElement={<Login {...{ setUser }} />} />
    </ThemeProvider>
  );
};

export default withStyles(styles)(App);
