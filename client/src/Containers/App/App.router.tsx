import React from "react";
import { createBrowserRouter } from "react-router-dom";
import Login from "../../components/Login/Login";
import Register from "../../components/Register/Register";
import HomePage from "../HomePage/HomePage";
import ProfilePage from "../ProfilePage/ProfilePage";
import { Movie, User } from "./App.types";
import Root from "./Root";
import RootWithChildren from "./Root.copy";

export const createViews = (user: User, setUser: React.Dispatch<React.SetStateAction<User>>, movies: Movie[]) =>
  createBrowserRouter([
    {
      path: '/',
      element: <RootWithChildren {...{ user, setUser }} children={<HomePage {...{ user, movies }} />} />,
    },
    {
      path: "/login",
      element: <RootWithChildren {...{ user, setUser }} children={<Login {...{ setUser }} />} />,
    },
    {
      path: "/register",
      element: <RootWithChildren {...{ user, setUser }} children={<Register {...{ setUser }} />} />,
    },
    {
      path: "/profile",
      element: <RootWithChildren {...{ user, setUser }} children={<ProfilePage {...{ user, setUser, movies }} />} />
    }
    // {
    //   path: '/',
    //   element: <Root {...{ user, setUser }} />,
    //   children: [
    //     {
    //       path: "",
    //       element: <HomePage {...{ user, movies }} />,
    //     },
    //     {
    //       path: "/login",
    //       element: <Login {...{ setUser }} />,
    //     },
    //     {
    //       path: "/register",
    //       element: <Register {...{ setUser }} />,
    //     },
    //     {
    //       path: "/profile",
    //       element: <ProfilePage {...{ user, movies }} />
    //     }
    //   ]
    // }
  ]);
