import { createStyles, Theme } from "@material-ui/core";

export const styles = (theme: Theme) =>
    createStyles({
        root: {
            height: '100vh',
            width: '100vw',
            backgroundColor: '#121212',
            overflow: 'hidden'
        },
    })