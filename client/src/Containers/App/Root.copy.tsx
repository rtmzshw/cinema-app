import { withStyles, WithStyles } from '@material-ui/core';
import React, { FunctionComponent, useState } from 'react';
import { Outlet, useLoaderData } from "react-router-dom";
import NavBar from '../../components/NavBar/NavBar';
import './App.css';
import { styles } from './App.styles';
import { User } from './App.types';

interface RootProps extends WithStyles<typeof styles> {
    user: User;
    setUser: React.Dispatch<React.SetStateAction<User>>;
    children: JSX.Element;
}

const RootWithChildren: FunctionComponent<RootProps> = (props) => {
    const { user, setUser, children, classes } = props;

    return (
        <div className={classes.root}>
            <NavBar {...{ user, setUser }} />
            <div style={{ height: "90vh", overflowY: "auto" }}>
                {children}
            </div>
        </div>
    );
};

export default withStyles(styles)(RootWithChildren);