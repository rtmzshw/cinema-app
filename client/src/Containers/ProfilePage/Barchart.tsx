// import { axisBottom, scaleBand, ScaleBand, select } from "d3";
// import React from "react";
// import { useEffect, useRef } from "react";

// interface Data {
//     label: string;
//     value: number;
// }

// interface BarChartProps {
//     data: Data[];
// }

// function BarChart({ data }: BarChartProps) {
//     const margin = { top: 0, right: 0, bottom: 0, left: 0 };
//     const width = 700 - margin.left - margin.right;
//     const height = 300 - margin.top - margin.bottom;
//     const scaleX = scaleBand()
//         .domain(data.map(({ label }) => label))
//         .range([0, width]);
//     return (
//         <svg
//             width={width + margin.left + margin.right}
//             height={height + margin.top + margin.bottom}
//         >
//             <g transform={`translate(${margin.left}, ${margin.top})`}></g>
//             <AxisBottom scale={scaleX} transform={`translate(0, ${height})`} />
//         </svg>
//     );
// }

// interface AxisBottomProps {
//     scale: ScaleBand<string>;
//     transform: string;
// }

// function AxisBottom({ scale, transform }: AxisBottomProps) {
//     const ref = useRef<SVGGElement>(null);

//     useEffect(() => {
//         if (ref.current) {
//             select(ref.current).call(axisBottom(scale));
//         }
//     }, [scale]);

//     return <g ref={ref} transform={transform} />;
// }

import { useEffect, useRef } from "react";
import {
  axisBottom,
  axisLeft,
  ScaleBand,
  scaleBand,
  ScaleLinear,
  scaleLinear,
  select
} from "d3";
import React from "react";

export interface IData {
    label: string;
    value: number;
  }
  
interface BarChartProps {
  data: IData[];
}

interface AxisBottomProps {
  scale: ScaleBand<string>;
  transform: string;
}

interface AxisLeftProps {
  scale: ScaleLinear<number, number, never>;
}

interface BarsProps {
  data: BarChartProps["data"];
  height: number;
  scaleX: AxisBottomProps["scale"];
  scaleY: AxisLeftProps["scale"];
}

function AxisBottom({ scale, transform }: AxisBottomProps) {
  const ref = useRef<SVGGElement>(null);

  useEffect(() => {
    if (ref.current) {
      select(ref.current).attr('color', "white").call(axisBottom(scale));
    }
  }, [scale]);

  return <g ref={ref} transform={transform} />;
}

function AxisLeft({ scale }: AxisLeftProps) {
  const ref = useRef<SVGGElement>(null);

  useEffect(() => {
    if (ref.current) {
      select(ref.current).attr('color', "white").call(axisLeft(scale));
    }
  }, [scale]);

  return <g ref={ref} />;
}

function Bars({ data, height, scaleX, scaleY }: BarsProps) {
  return (
    <>
      {data.map(({ value, label }) => (
        <rect
          key={`bar-${label}`}
          x={scaleX(label)}
          y={scaleY(value)}
          width={scaleX.bandwidth()}
          height={height - scaleY(value)}
          fill="#1d54ff"
        />
      ))}
    </>
  );
}

export function BarChart({ data }: BarChartProps) {
  const margin = { top: 10, right: 0, bottom: 20, left: 30 };
  const width = 900 - margin.left - margin.right;
  const height = 300 - margin.top - margin.bottom;

  const scaleX = scaleBand()
    .domain(data.map(({ label }) => label))
    .range([0, width])
    .padding(0.5);
    
  const scaleY = scaleLinear()
    .domain([0, Math.max(...data.map(({ value }) => value))])
    .range([height, 0]);

  return (
    <svg
      width={width + margin.left + margin.right}
      height={height + margin.top + margin.bottom}
    >
      <g transform={`translate(${margin.left}, ${margin.top})`}>
        <AxisBottom scale={scaleX} transform={`translate(0, ${height})`} />
        <AxisLeft scale={scaleY} />
        <Bars data={data} height={height} scaleX={scaleX} scaleY={scaleY} />
      </g>
    </svg>
  );
}
