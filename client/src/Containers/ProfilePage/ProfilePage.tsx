import { TextField, withStyles, WithStyles } from "@material-ui/core";
import moment from "moment";
import React, { FunctionComponent, useEffect, useRef, useState } from "react";
import InputCell from "../../components/InputCell/InputCell";
import SubTitle from "../../components/SubTitle/SubTitle";
import { getAllOrders, getUserOrderHistory } from "../../api/order";
import { getScreenings } from "../../api/screening";
import { Movie, Order, User } from "../App/App.types";
import { styles } from "./ProfilePage.styles";
import * as d3 from "d3";
import RtlTextField from "../../components/RtlTextField/RtlTextField";
import ManagerActions from "./ManagerActions";
import EditIcon from '@material-ui/icons/Edit';
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";
import { updateUser } from "../../api/user";
import { axisBottom, ScaleBand, scaleBand, select } from "d3";
import { BarChart, IData } from "./Barchart";
import { getMovies } from "../../api/movie";
import { groupBy, indexBy } from "ramda";

export interface ProfilePageProps extends WithStyles<typeof styles> {
    user: User;
    setUser: React.Dispatch<React.SetStateAction<User>>;
    movies: Movie[];
}

const ProfilePage: FunctionComponent<ProfilePageProps> = props => {
    const { user, setUser, movies, classes } = props;
    const [userData, setUserData] = useState<{ firstName: string, lastName: string }>({ firstName: user.firstName, lastName: user.lastName });
    const [editMode, setEditMode] = useState<boolean>(false);
    const [orders, setOrders] = useState<Order[]>([]);
    const [graphData, setGraphData] = useState<IData[]>([]);

    useEffect(() => {
        const getOrders = async () => {
            if (user.role === "client") {
                const o = await getUserOrderHistory(user._id);
                setOrders(o);
            } else {
                const orders = await getAllOrders();
                const graphData = await getGraphData();
                setOrders(orders);
                setGraphData(graphData)
            }
        }

        getOrders();

    }, [user._id])

    const saveUserDataChanges = async () => {
        const { firstName, lastName } = userData;
        await updateUser(user._id, { firstName, lastName });
        setUser({...user, firstName, lastName});
        setEditMode(false);
    }

    const getGraphData = async () => {
        const [movies, screenings] = await Promise.all([getMovies(),getScreenings()]);
        const moviesById = indexBy((m => m._id), movies);
        const screeningWithName = screenings.map(s => ({ ...s, name: moviesById[s.movieId].name }));
        const screeningByCount = groupBy(((s) => s.name), screeningWithName);
        return Object.keys(screeningByCount).map((screeningName) => ({ label: screeningName, value: screeningByCount[screeningName].length }))
    }

    return (
        <div>
            <SubTitle title="הפרופיל שלי" children={
                <IconButton onClick={e => setEditMode(true)}>
                    <EditIcon />
                </IconButton>
            } />
            <div className={classes.userDetails}>
                {editMode && <Button variant="contained" color="primary" onClick={saveUserDataChanges} style={{ marginTop: "auto" }}>שמור שינויים</Button>}
                <InputCell title="כתובת מייל">
                    <RtlTextField disabled value={user._id} />
                </InputCell>
                <InputCell title="שם משפחה">
                    <RtlTextField disabled={!editMode} onChange={({ target: { value } }) => setUserData({ ...userData, lastName: value })} value={userData.lastName} />
                </InputCell>
                <InputCell title="שם פרטי">
                    <RtlTextField disabled={!editMode} onChange={({ target: { value } }) => setUserData({ ...userData, firstName: value })} value={userData.firstName} />
                </InputCell>
            </div>
            {user.role === "admin" &&
                <div>
                    <SubTitle title="פעולות מנהל" />
                    <ManagerActions {...{ movies, user }} />
                </div>
            }
            <SubTitle title="היסטוריית הזמנות" />
            {user.role === "admin" ?
                <div>
                    <BarChart data={graphData} ></BarChart>
                </div>
                :
                <div>
                    {orders.map(order =>
                        <div key={order._id} className={classes.userDetails} style={{ padding: "20px" }}>
                            <InputCell title="מספר הזמנה">
                                <RtlTextField disabled value={order._id} />
                            </InputCell>
                            <InputCell title="תאריך הזמנה">
                                <RtlTextField disabled value={moment(order.date).format("DD/MM")} />
                            </InputCell>
                        </div>
                    )}
                </div>
            }
        </div>
    )
};

export default withStyles(styles)(ProfilePage);

