import { createStyles, Theme } from "@material-ui/core";

export const styles = (theme: Theme) =>
    createStyles({
        userDetails: {
            display: "flex",
            justifyContent: "space-around",
            width: "50%",
            marginLeft: "auto"
        },
        btn: {
            fontFamily: 'inherit',
            width: '10%',
            alignSelf: 'flex-end'
        },
        accordion: {
            marginLeft: "auto !important",
            width: "60%"
        },
        accordions: {
            padding: "20px"
        },
        accordionDetails: {
            display: "flex",
            alignItems: "center",
            justifyContent: "flex-end"
        }
    })