import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Button,
  MenuItem,
  withStyles,
  WithStyles,
} from "@material-ui/core";
import Select from "@material-ui/core/Select";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { isEmpty } from "ramda";
import React, { FunctionComponent, useState } from "react";
import Swal from "sweetalert2";
import { deleteMovie, postMovie } from "../../api/movie";
import { cancelOrder } from "../../api/order";
import {
  deleteScreening,
  postScreening,
  PostScreeningRequestBody,
} from "../../api/screening";
import Filters from "../../components/FindScreening/Filters";
import InputCell from "../../components/InputCell/InputCell";
import RtlTextField from "../../components/RtlTextField/RtlTextField";
import { calcDayRange } from "../../utils";
import { Movie, MOVIE_KEYS, User } from "../App/App.types";
import { styles } from "./ProfilePage.styles";

export interface ManagerActionsProps extends WithStyles<typeof styles> {
  movies: Movie[];
  user: User;
}

const ManagerActions: FunctionComponent<ManagerActionsProps> = (props) => {
  const { movies, classes } = props;
  const [getScreeningsQuery, setGetScreeningsQuery] =
    useState<PostScreeningRequestBody>({
      isVip: false,
      movieId: "",
      theatreSize: "m",
      time: calcDayRange(new Date()),
    });
  const [deleteScreeningId, setDeleteScreeningId] = useState<string>("");
  const [newMovie, setNewMovie] = useState<Omit<Movie, "_id">>({
    name: "",
    rating: 0,
    src: "",
    price: 0,
    year: 0,
  });
  const [movieToDelete, setMovieToDelete] = useState<{
    _id: string;
    name: string;
  }>({ _id: "", name: "" });
  const [deleteOrderId, setDeleteOrderId] = useState<string>("");

  const moviePropsToDisplayName: Record<keyof Omit<Movie, "_id">, string> = {
    price: "מחיר לכרטיס",
    rating: "דירוג",
    src: "קישור לתמונה",
    year: "שנת פרסום",
    name: "שם הסרט",
  };

  const sendOrderBtn = (
    <Button
      onClick={(e) => {
        postScreening({ ...getScreeningsQuery });
        Swal.fire("הוספת הקרנה בוצעה בהצלחה");
      }}
      disabled={isEmpty(getScreeningsQuery)}
      className={classes.btn}
      variant="contained"
      color="primary"
    >
      הוסף
    </Button>
  );

  const accordionSummary = (title: string) => (
    <AccordionSummary
      style={{ direction: "rtl" }}
      expandIcon={<ExpandMoreIcon />}
    >
      {title}
    </AccordionSummary>
  );

  return (
    <div className={classes.accordions}>
      <Accordion className={classes.accordion}>
        {accordionSummary("הוספת הקרנה")}
        <AccordionDetails className={classes.accordionDetails}>
          <Filters
            style={{ width: "70%" }}
            {...{
              getScreeningsQuery,
              setGetScreeningsQuery,
              movies,
              actionButton: sendOrderBtn,
              action: "post",
            }}
          />
          <InputCell title="גודל אולם">
            <Select
              variant="outlined"
              value={getScreeningsQuery.theatreSize}
              onChange={(e) =>
                setGetScreeningsQuery({
                  ...getScreeningsQuery,
                  theatreSize: e.target.value as any,
                })
              }
            >
              <MenuItem value={"s"}>קטן</MenuItem>
              <MenuItem value={"m"}>בינוני</MenuItem>
              <MenuItem value={"l"}>גדול</MenuItem>
            </Select>
          </InputCell>
        </AccordionDetails>
      </Accordion>
      <Accordion className={classes.accordion}>
        {accordionSummary("מחיקת הקרנה")}
        <AccordionDetails className={classes.accordionDetails}>
          <Button
            variant="contained"
            color="primary"
            onClick={(e) => deleteScreening(deleteScreeningId)}
          >
            מחק
          </Button>
          <InputCell title="מזהה הקרנה">
            <RtlTextField
              style={{ paddingLeft: "10px" }}
              value={deleteScreeningId}
              onChange={({ target: { value } }) => setDeleteScreeningId(value)}
            />
          </InputCell>
        </AccordionDetails>
      </Accordion>
      <Accordion className={classes.accordion}>
        {accordionSummary("הוספת סרט")}
        <AccordionDetails className={classes.accordionDetails}>
          <Button
            variant="contained"
            color="primary"
            onClick={(e) => {postMovie(newMovie)
                Swal.fire("הסרט נוסף בהצלחה");
            }}
          >
            הוסף
          </Button>
          {MOVIE_KEYS.map((movieKey) => (
            <InputCell key={movieKey} title={moviePropsToDisplayName[movieKey]}>
              <RtlTextField
                style={{ paddingLeft: "15px" }}
                value={newMovie[movieKey]}
                onChange={({ target: { value } }) =>
                  setNewMovie({ ...newMovie, [movieKey]: value })
                }
              />
            </InputCell>
          ))}
        </AccordionDetails>
      </Accordion>
      <Accordion className={classes.accordion}>
        {accordionSummary("מחיקת סרט")}
        <AccordionDetails className={classes.accordionDetails}>
          <Button
            variant="contained"
            color="primary"
            onClick={(e) => deleteMovie(movieToDelete._id)}
            style={{ marginTop: "auto" }}
          >
            מחק
          </Button>
          <InputCell
            title="בחר סרט למחיקה"
            style={{ width: "20%", paddingLeft: "10px" }}
          >
            <Select
              style={{ width: "100%" }}
              value={movieToDelete?.name ?? ""}
              variant="outlined"
              onChange={(event) => {
                const newMovie = movies.find(
                  ({ name }) => name === event.target.value
                );
                if (newMovie) {
                  const { _id, name } = newMovie;
                  setMovieToDelete({ _id, name });
                }
              }}
            >
              {movies.map(({ name }) => {
                return (
                  <MenuItem key={name} value={name}>
                    {name}
                  </MenuItem>
                );
              })}
            </Select>
          </InputCell>
        </AccordionDetails>
      </Accordion>
      <Accordion className={classes.accordion}>
        {accordionSummary("ביטול הזמנה")}
        <AccordionDetails className={classes.accordionDetails}>
          <Button
            variant="contained"
            color="primary"
            onClick={(e) => cancelOrder(deleteOrderId)}
          >
            בטל
          </Button>
          <InputCell title="מזהה הזמנה">
            <RtlTextField
              style={{ paddingLeft: "10px" }}
              value={deleteOrderId}
              onChange={({ target: { value } }) => setDeleteOrderId(value)}
            />
          </InputCell>
        </AccordionDetails>
      </Accordion>
    </div>
  );
};

export default withStyles(styles)(ManagerActions);
