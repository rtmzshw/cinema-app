import moment from "moment";
import { ScreeningRequestQuery } from "./api/screening";

export const copyArr = (arr: any[][]) => arr.map(r => r.map(c => c));

export const calcDayRange = (date: Date): { min: number; max: number } => {
    const min = date.setHours(0, 0, 0)
    const max = moment(date).add(1, 'day').toDate().getTime();
    return { min, max };
}

export const isPastDate = (getScreeningsQuery: ScreeningRequestQuery) =>
    moment(getScreeningsQuery.time?.max).isBefore(Date.now())